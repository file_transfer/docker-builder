FROM docker:latest

RUN apk add --update python3 py3-pip \
    && pip3 install --upgrade pip

# Pinning docker compose 1.23 as 1.24 requires gcc
RUN pip3 install awscli docker-compose~=1.23.0
