# Docker-builder

This container provides the following for CI pipelines:

* Allows for building docker containers
* Has docker compose for running integration tests
* Has the AWS-CLI for uploading containers to AWS ECR

<img class="statcounter" src="https://c.statcounter.com/11994331/0/eed1a29e/1/">